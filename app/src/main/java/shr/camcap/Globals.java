package shr.camcap;

/**
 * Author: Henrik Oddløkken on 28.09.2015.
 *
 * This file contains the global variables for this project.
 *
 * Properties:
 *
 *      - nrOfPhotos: The number of photos keeps track of the stored photos
 *                    stored in the database. This is useful to check if the
 *                    album has any contents, otherwise the user cannot access
 *                    the album activity. Additionally it's easy to loop through
 *                    all the photos when displaying the photos in the gallery.
 *
 */
public class Globals {

    // Properties:
    private static int nrOfPhotos = 0;  // Count the number of photos in the database.

    // Constructor:
    Globals(){ /* Empty constructor.*/ }

    // Getters and setters:

    public static int getNrOfPhotos() {
        return nrOfPhotos;
    }

    public static void setNrOfPhotos(int nrOfPhotos) {
        Globals.nrOfPhotos = nrOfPhotos;
    }
}
