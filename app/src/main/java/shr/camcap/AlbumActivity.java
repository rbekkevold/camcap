package shr.camcap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.support.v4.view.GestureDetectorCompat;

import java.io.IOException;


/**
 * Authors: Susann Lundekvam, Robin Bekkevold and Henrik Oddløkken
 * Created on 25.09.2015.
 *
 * This file contains the Album where the user can browse through
 * the images that has been captured with the camera.
 *
 * Each Photo object has a photo file, city name where the
 * photo was captured and a timestamp for when the photo was taken.
 * This information is displayed for each Photo.
 *
 *
 * The possible methods:
 *
 *  - On swipe (onFling) left and right moves to the next or previous pictures.
 *
 */
public class AlbumActivity extends AppCompatActivity
        implements GestureDetector.OnGestureListener {

    DBHandler db;
    Constants constants = new Constants();
    Globals globals = new Globals();

    // Declare view variables to be displayed:
    ImageView displayPhoto;
    TextView  displayIndex;
    TextView  displayCity;
    TextView  displayDate;

    Bitmap bitmap;

    private GestureDetectorCompat gestureDetector;

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    // Index to keep track of the current image displayed.
    // Set to 0 to display the first Photo object:
    private int index = 0;

    // Array from the database that keeps all the Photo objects:
    public shr.camcap.Photo[] photoList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        this.gestureDetector = new GestureDetectorCompat(this, this);

        // Set ID reference to the variables to be displayed:

        displayPhoto = (ImageView) findViewById(R.id.displayPhoto);
        displayIndex = (TextView)  findViewById(R.id.displayIndex);
        displayCity  = (TextView)  findViewById(R.id.displayCity);
        displayDate  = (TextView)  findViewById(R.id.displayDate);

        // Initialize database:
        db = new DBHandler(this, null, null, 1);

        // update photos from database:
        db.updatePhotoArray();

        // Get photo list array:
        photoList = db.getPhotoList();

        // Display first image:
        setImage();

    }

/*---------------------------------------------------------------------------*/
/*                             SET / UPDATE IMAGE
/*---------------------------------------------------------------------------*/

    // Go to next or first photo:

    public void next() {
        int curIndex = getIndex();
        int maxIndex = globals.getNrOfPhotos();
        int index = curIndex += 1;

        if (curIndex == maxIndex)
            index = 0;

        setIndex(index);

        setImage();

    }

    // Go to previous or last photo:

    public void prev() throws IOException {
        int curIndex = getIndex();
        int maxIndex = globals.getNrOfPhotos();

        int index;

        if(curIndex == 0)
            index = maxIndex-1;
        else
            index = curIndex -1;

        setIndex(index);
        setImage();
    }

    // Update the displayed UI elements based on the current index:

    public void setImage(){

        int index = getIndex();

        String photoFile = constants.getPicturePath() +
                photoList[index].getPhotoFile() + ".jpg";

        int maxIndex = globals.getNrOfPhotos();

        // Display the number of the current and number of captured photos:
        displayIndex.setText(index + 1 + " of " + globals.getNrOfPhotos());

        // Create a bitmap object of the photo:
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        bitmap = BitmapFactory.decodeFile(photoFile, options);

        // Display the current photo:
        displayPhoto.setImageBitmap(bitmap);

        rotate(photoFile);

        // Display the city for where the photo was captured:
        displayCity.setText(photoList[index].getCity());

        // Display the timestamp for when the photo was taken:
        displayDate.setText(photoList[index].getTime());
    }

    // Rotate the image:
    /**
    * Get the orientation of the view Image:
    * Credit: http://stackoverflow.com/questions/2284851/how-can-i-find-the-orientation-of-a-picture-taken-with-intent-mediastore-action
    */
    public void rotate(String photoFile){

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(photoFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientationString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);

        int orientation = Integer.parseInt(orientationString);

        // Logged the photos orientation value to write this code:
        if(orientation == 6)
            displayPhoto.setRotation(90);
        else if(orientation == 8)
            displayPhoto.setRotation(-90);
        else if(orientation == 3)
            displayPhoto.setRotation(180);
        else
            displayPhoto.setRotation(0);
    }

/*---------------------------------------------------------------------------*/
/*                             GETTERS AND SETTERS
/*---------------------------------------------------------------------------*/

    public int getIndex(){
        return index;
    }

    public void setIndex(int i) {
        this.index = i;
    }

/*---------------------------------------------------------------------------*/
/*                             S W I P I N G
/*---------------------------------------------------------------------------*/
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    /**
     * When the user swipes left this gesture calls the next() method, which goes to the next image,
     * when the user swipes right the prev() method gets called and goes to the previous image.
     *
     * Credit:
     * http://stackoverflow.com/questions/4139288/android-how-to-handle-right-to-left-swipe-gestures
     */
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        boolean result = false;
        try {
            float diffY = e2.getY() - e1.getY();
            float diffX = e2.getX() - e1.getX();
            if (Math.abs(diffX) > Math.abs(diffY)) {
                if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffX > 0) {
                        prev();
                    } else {
                        next();
                    }
                }
                result = true;
            }
            result = true;

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }
}