package shr.camcap;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;

import java.io.File;

/**
 * Author: Henrik Oddløkken on 25.09.2015.
 *
 * This file contains the database for this project.
 *
 * The database has one table; Pictures, that stores the saved photos.
 * Along with the photo it also stores the city where the photo was taken
 * and the timestamp for when the photo was taken.
 *
 * The possible methods:
 *
 *  - savePhoto(String pictureFile, String cityName, String time)
 *      - Adds a new row to the table with the passed arguments.
 *
 *  - clearDB(File path)
 *      - Clears all the rows in the table.
 *      - Deletes the directory where the photo files is stored.
 *
 *  - updatePhotoArray()
 *       - Takes all rows from the database and creates a Photo object for each row.
 *         Then it adds each Photo object into an array which we use later in
 *         the album activity, ready to display the content.
 *
 * Got the code from theNewBoston's tutorial to create the database:
 * Youtube Tutorial (49-54): URL: https://www.youtube.com/playlist?list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
 * Source Code:              URL: https://www.thenewboston.com/forum/topic.php?id=3767
 *
 */

public class DBHandler extends SQLiteOpenHelper {

    private static final int    DATABASE_VERSION    = 4;
    private static final String DATABASE_NAME       = "PicturesDB.db";
    public static final  String TABLE_PICTURES      = "pictures";
    public static final  String COLUMN_ID           = "id";
    public static final  String COLUMN_PICTURE_SRC  = "picture_src";
    public static final  String COLUMN_CITY         = "city";
    public static final  String COLUMN_TIME         = "timeStamp";

    Constants   constants   = new Constants();
    Globals     globals     = new Globals();

    // Array to store the Photo objects:
    public Photo[] photoList = new Photo[constants.getMaxImages()];

    // We need to pass database information along to superclass
    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_PICTURES + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_PICTURE_SRC + " VARCHAR(64), " +
                COLUMN_CITY + " VARCHAR(32), " +
                COLUMN_TIME + " VARCHAR(32) " +
                ");";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PICTURES);
        onCreate(db);
    }

    // Add a new row to the database:

    public void savePhoto(String pictureFile, String cityName, String time){
        ContentValues values = new ContentValues();
        values.put(COLUMN_PICTURE_SRC, pictureFile);
        values.put(COLUMN_CITY, cityName);
        values.put(COLUMN_TIME, time);
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_PICTURES, null, values);
        db.close();
    }

    // Clear all rows in table photo
    // and delete the directory where the files are stored:

    public boolean clearDB(File path){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_PICTURES, null, null);
        db.close();

        /**
         * Empty the directory before deleting the directory itself.
         *
         * Credit: http://stackoverflow.com/questions/5701586/delete-a-folder-on-sd-card
         */
        if(path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }
            for(int i = 0; i < files.length; i++) {
                if(files[i].isDirectory()) {
                    clearDB(files[i]);
                }
                else {
                    files[i].delete();
                }
            }
        }
        return( path.delete() );

    }

    // Get the rows from the database and set them as Photo object.
    // Add all Photo objects into an array to access them.
    public void updatePhotoArray(){

        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * " + "FROM " + TABLE_PICTURES + " WHERE 1 ORDER BY timeStamp";

        // Cursor points to a location in your results
        Cursor c = db.rawQuery(query, null);

        // Move to the first row in your results
        c.moveToFirst();

        // Index to iterate through the photos.
        int index = 0;

        // Loop through all rows in table photo:
        while (!c.isAfterLast()) {

            // Get values from this row:
            String pictureFile = c.getString(c.getColumnIndex("picture_src"));
            String city = c.getString(c.getColumnIndex("city"));
            String time = c.getString(c.getColumnIndex("timeStamp"));

            if(city == null)
                city = "Unknown";

            // Adds photo into array:
            photoList[index] = new Photo(pictureFile, city, time);

            index++;                        // Update index.
            c.moveToNext();                 // Go to next row.
        }

        c.close();                          // Close cursor.
        db.close();                         // Close db.

        globals.setNrOfPhotos(index);       // Set the global value of saved photos.
    }

    // Get photoList array:
    public shr.camcap.Photo[] getPhotoList() {
        return photoList;
    }
}
