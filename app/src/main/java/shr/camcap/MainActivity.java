package shr.camcap;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;


/**
 * Authors: Susann Lundekvam and Robin Bekkevold
 * Created on 22.09.2015.
 *
 * This is the activity that launches when the app starts.
 * This handles starting a camera intent, and also saves the picture taken on the sd card and information
 * about the GPS location of the user, a timestamp, the filename in the database, when the picture
 * comes back from the camera intent.
 *
 * Camera intent integration learned from: https://www.youtube.com/watch?v=6Z6k7X2vfhk
 */
public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

        private static final String TAG = "mainActivity";

        private File imageFile;
        private File folder;
        private String filename;

        Constants constants;
        Globals globals;
        DBHandler db;

        MainActivity mainActivity = this;

        private GoogleApiClient mLocationClient;
        public double current_lat;
        public double current_long;
        public String city = "Unknown";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);
                constants = new Constants();

                db = new DBHandler(this, null, null, 1);

                db.updatePhotoArray();

                Button clearDB = (Button)findViewById(R.id.clearDB);

                folder = new File(constants.getPicturePath());
                // create the folder CamCap; this is where we'll store the images
                if (!folder.exists()) { folder.mkdir(); }

                // clear database:
                clearDB.setOnClickListener(
                        new Button.OnClickListener() {
                                public void onClick(View v) {

                                        File dir = new File(constants.getPicturePath());
                                        db.clearDB(dir);

                                        Toast.makeText(mainActivity, "Cleared whole database", Toast.LENGTH_LONG).show();
                                }
                        }
                );

                if (servicesOK()) {
                        mLocationClient = new GoogleApiClient.Builder(this)
                                        .addApi(LocationServices.API)
                                        .addConnectionCallbacks(this)
                                        .addOnConnectionFailedListener(this)
                                        .build();

                        mLocationClient.connect();
                } else {
                        Toast.makeText(this, "Can't connect to GPS service.", Toast.LENGTH_SHORT).show();
                }

        }


        public void startCamera(View view) {
                // if the device has a camera feature
                if (hasCamera()) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        String randomFilename = generateRandomFilename();
                        setFilename(randomFilename);

                        folder = new File(constants.getPicturePath());
                        // create the folder CamCap; this is where we'll store the images
                        if (!folder.exists()) { folder.mkdir(); }

                        // arguments: directory to store the file, filename
                        imageFile = new File(
                                        constants.getPicturePath(), randomFilename + ".jpg"
                        );

                        Uri tempUri = Uri.fromFile(imageFile);

                        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
                        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1); // quality range from 0 to 1

                        startActivityForResult(intent, 1);

                        setCurrentLocation();
                        setCity();
                } else {
                        Toast.makeText(this, "This device does not have a camera. Boooo.", Toast.LENGTH_LONG).show();
                }
        }

        private boolean hasCamera() {
                return getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        }

        /**
         * Generate a random string as the file name. Credit:
         * http://stackoverflow.com/questions/5683327/how-to-generate-a-random-string-of-20-characters
         */
        public String generateRandomFilename() {
                char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
                StringBuilder sb = new StringBuilder();
                Random random = new Random();
                for (int i = 0; i < 20; i++) {
                        char c = chars[random.nextInt(chars.length)];
                        sb.append(c);
                }

                return sb.toString();
        }


        /**
         * This method captures the image coming back from the camera app.
         */
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
                super.onActivityResult(requestCode, resultCode, data);

                if (requestCode == 1) {
                        switch (resultCode) {
                                case Activity.RESULT_OK:
                                        if (imageFile.exists()) {
                                                if(globals.getNrOfPhotos() + 1 < constants.getMaxImages()) {

                                                        // Get the timestamp:
                                                        String time = new SimpleDateFormat("dd MMM yyyy \n kk:mm", Locale.US).format(new Date());

                                                        // Save picture and location to database:
                                                        db.savePhoto(getFilename(), city, time);

                                                        // Initialize photos from database:
                                                        db.updatePhotoArray();

                                                        Toast.makeText(this, "The file was saved at " + imageFile.getAbsolutePath(), Toast.LENGTH_LONG).show();
                                                } else {
                                                        Toast.makeText(this, "ERROR: The database is full", Toast.LENGTH_LONG).show();
                                                }

                                        } else {
                                                Toast.makeText(this, "There was an error saving the file.", Toast.LENGTH_LONG).show();
                                        }
                                break;
                                case Activity.RESULT_CANCELED:
                                        Toast.makeText(this, "Am I not pretty enough for you? -image", Toast.LENGTH_LONG).show();
                                break;
                        }
                }
        }

        // Go to album activity:
        public void goToAlbum(View view) {
                db.updatePhotoArray();
                if(globals.getNrOfPhotos() > 0) {
                        Intent nextScreen = new Intent(getApplicationContext(), AlbumActivity.class);
                        startActivity(nextScreen);
                }else{
                        Toast.makeText(this, "No images.. IT'S A GIANT PROBLEM!!!", Toast.LENGTH_LONG).show();
                }
        }


        /**
         * Method to check if it's possible to connect to the Google Play Service.
         */
        public boolean servicesOK() {
                int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

                if (isAvailable == ConnectionResult.SUCCESS) {
                        return true;
                } else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable)) {
                        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isAvailable, this, 9001);
                        dialog.show();
                } else {
                        Toast.makeText(this, "Can't connect to mapping service.", Toast.LENGTH_SHORT).show();
                }

                return false;
        }

        /**
         * Set the users current latitude and longitude.
         */
        public void setCurrentLocation() {
                Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mLocationClient);

                if (currentLocation == null) {
                        Toast.makeText(this, "Could not connect..", Toast.LENGTH_SHORT).show();
                } else {
                        current_lat = currentLocation.getLatitude();
                        current_long = currentLocation.getLongitude();
                }
        }

        /**
         * Make a geo search for the user location name by using the current
         * latitude and longitude properties of this class.
         */
        public void setCity() {
                Geocoder geoc = new Geocoder(this, Locale.getDefault());
                try {
                        List<Address> list = geoc.getFromLocation(current_lat, current_long, 1);

                        if (list.size() > 0) {
                                Address addr = list.get(0);

                                city = addr.getLocality();

                                Toast.makeText(this, "Taken in: " + city, Toast.LENGTH_SHORT).show();
                        }
                } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(this, "Could not find your location.", Toast.LENGTH_LONG).show();
                }
        }

        @Override
        public void onConnected(Bundle bundle) {}

        @Override
        public void onConnectionSuspended(int i) {
                Toast.makeText(this, "Connection suspended.", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
                Toast.makeText(this, "Connection failed.", Toast.LENGTH_SHORT).show();
        }


        // Getters and setters:

        public String getFilename() {
                return filename;
        }

        public void setFilename(String filename) {
                this.filename = filename;
        }




} // end of MainActivity class
