package shr.camcap;

/**
 * Author: Henrik Oddløkken on 25.09.2015.
 *
 * This file contains the Photo data to handle the photos.
 *
 * When the program reads from the database it creates a new object of
 * this Photo class and puts it into an array. That way we can access
 * the class object's properties from the array.
 *
 */
public class Photo {

    // Properties:

    private String photoFile;   // The filename of the photo.
    private String city;        // The city where the photo was taken.
    private String time;        // The current time when the photo was taken.


    // Constructor:

    public Photo(String photoFile, String city, String time){
        this.photoFile = photoFile;
        this.city = city;
        this.time = time;
    }

    // Getters:

    public String getPhotoFile(){
        return photoFile;
    }

    public String getCity(){
        return city;
    }

    public String getTime(){
        return time;
    }
}
