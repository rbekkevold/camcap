package shr.camcap;

import android.os.Environment;

/**
 * Author Henrik Oddløkken on 25.09.2015.
 *
 * This file contains the constants for this project.
 *
 * Properties:
 *
 *      - Get the path for the directory "CamCap" where photos is stored at the device.
 *      - Get the max value of number of photos that can be stored.
 */
public class Constants {

    // Properties:

    // The device's path to CamCap directory.
    private static final String PICTURE_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/CamCap/";

    // Max photos the user can capture.
    private static final int MAX_PHOTOS = 1000;


    // Constructor:
    public Constants(){ /* Empty constructor. */ }

    // Getters:

    public String getPicturePath(){
        return PICTURE_PATH;
    }

    public static int getMaxImages() {
        return MAX_PHOTOS;
    }



}
